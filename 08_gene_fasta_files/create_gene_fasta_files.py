import os
from itertools import islice
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import argparse

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description="Process some integers.")

# Add arguments with default values as the current directory
parser.add_argument("busco_folder", type=str, help="The path to the busco folder", default=os.getcwd())
parser.add_argument("genes_folder", type=str, help="The path to the genes folder", default=os.getcwd())

# Parse the arguments
args = parser.parse_args()

subpath = "run_apicomplexa_odb10"
taxa_list = ["Ht", "Pb", "Pc", "Pf", "Pk", "Pv", "Py", "Tg"]

sequences = {}  # Create an empty dictionary to store the sequences

for taxon in taxa_list:
    sequences[taxon] = {}  # Create a new dictionary for this taxon
    file_path = os.path.join(args.busco_folder, taxon, subpath, "full_table.tsv")
    with open(file_path, "r") as tsv:
        # Skip the first 3 lines
        for line in islice(tsv, 3, None):
            components = line.strip().split("\t")
            busco_id = components[0]
            status = components[1]
            # Check if the status is 'Complete'
            if status.lower() == "complete":
                sequence = components[2]
                # Add the sequence to the dictionary for this taxon
                sequences[taxon][busco_id] = sequence
            # Add duplicated BUSCOs from Tg
            if taxon == "Tg" and status.lower() == "duplicated":
                sequence = components[2]
                # Add the sequence to the dictionary for this taxon
                sequences[taxon][busco_id] = sequence

# Create a file for each subdictionary
for taxon, subdict in sequences.items():
    # Overwrite the file if it already exists
    with open(f"{taxon}_sequences.txt", "w") as file:
        for busco_id, sequence in subdict.items():
            file.write(f"{busco_id}\t{sequence}\n")

# Create a set with all keys from the first dictionary
common_keys = set(sequences[taxa_list[0]].keys())

# Intersect this set with the keys from all other dictionaries
for taxon in taxa_list[1:]:
    common_keys &= set(sequences[taxon].keys())

# Create a new dictionary with these common keys
common_dict = {key: {} for key in common_keys}

# Fill the new dictionary with the values from the original dictionaries
for key in common_keys:
    for taxon in taxa_list:
        common_dict[key][taxon] = sequences[taxon][key]

# Print the common_dict to a file
with open("common_sequences.txt", "w") as file:
    for key, subdict in common_dict.items():
        file.write(f"{key}\n")
        for taxon, sequence in subdict.items():
            file.write(f"\t{taxon}: {sequence}\n")

genes = {}

for taxon in taxa_list:
    genes[taxon] = {}
    fasta_file_path = os.path.join(args.genes_folder, f"{taxon}.faa")
    for record in SeqIO.parse(fasta_file_path, "fasta"):
        genes[taxon][record.id] = record.seq

# Create a file for each key in common_dict
for busco_id, taxon_dic in common_dict.items():
    with open(f"{busco_id}.faa", "w") as file:
        for taxon in taxa_list:
            gene_code = taxon_dic[taxon]
            sequence = genes[taxon][taxon_dic[taxon]]
            record = SeqRecord(seq=sequence, id=taxon, description="")
            SeqIO.write(record, file, "fasta")
