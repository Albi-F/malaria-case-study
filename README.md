# Malaria Case Study

## Plasmodium

### Gene prediction

Using GeneMark:

```bash
nohup gmes_petap.pl --ES --cores 10 --min_contig 10000 --sequence ../00_resources/Plasmodium_berghei.genome &

-min_contig 10000 -->  the genome of Plasmodium berghei is very fragmented and the default contig limit of GeneMark (50 000) is too high
--ES --> use self-training algorithm
--cores 10
```

## Haemoproteus tartakovskyi

### Clean Genome Sequence

Use the removeScaffold python script to remove scaffolds whose length is less than 3000 nucleotides and those whose GC content is lower than 30%.
The files we are working on have already been partially assembled, scaffolds are made of thousands if not millions of reads. We are discarding the scaffolds 

```bash
./removeScaffold.py ../00_resources/Haemoproteus_tartakovskyi.raw.genome 30 Haemoproteus_tartakovskyi.cleaned.genome 3200
```
30 because I lloked at the plot on the pdf, 3200 because it is close to the recommended number on the pdf

## Gene Prediction

It is probable that there are still some scaffolds fromthe bird, especially short ones, because in that case the GC content may be more varied and difficult to filter out.

```bash
nohup gmes_petap.pl --ES --cores 10 --min_contig 10000 --sequence ../02_Haemoproteus_clean_genome_sequence/Haemoproteus_tartakovskyi.cleaned.genome
```

If I ran it without `--min_contig` I get the following error:
```
error, input sequence size is too small data/training.fna: 64494
```

### Convert to a fasta file

The `-c` flag is recommended when working with files produced with GeneMark. The `-c` flag requires `-p`.

```bash
./gffParse.pl -c -p -i ../02_Haemoproteus_clean_genome_sequence/Haemoproteus_tartakovskyi.cleaned.genome -g Haemoproteus.gtf 
```

The above script returns the error below:

```
Use of uninitialized value $oldScaffold in hash element at ./gffParse.pl line 368, <GENOME> line 4226.
Use of uninitialized value within %scaffoldIndex in array element at ./gffParse.pl line 368, <GENOME> line 4226.
Use of uninitialized value $oldScaffold in concatenation (.) or string at ./gffParse.pl line 376, <GENOME> line 4226.
ERROR: First position of 3086 in CDS of gene 1_g is outside the boundaries of scaffold .
```

Use the following code to clean the headers of the gtf file so that `gffParser` works. I think that the `gffParser` program is not able to recognize the headers correctly.

```bash
cat Haemoproteus.gtf | sed "s/ GC=.*\tGeneMark.hmm/\tGeneMark.hmm/" > Ht2.gff
```

Create the fasta file with the sequences:

```bash
./gffParse.pl -c -p -i ../02_Haemoproteus_clean_genome_sequence/Haemoproteus_tartakovskyi.cleaned.genome -g Ht2.gff
```

### Search the genes for avian genes

For this I used blastp (which should be faster than blastx given that the six translations are not needed) and the database SwissProt.
Use the default output format so that it is possible to parse it with `datParser.py`.

```bash
blastp -query ../03_Haemoproteus_gene_prediction/gffParse.faa -db SwissProt -num_threads 20 -out Haemoproteus_blastp
```

To generate the list of genes to remove I used the `datParser.py` program as follows:
```bash
python datParser.py Haemoproteus_blastp ../03_Haemoproteus_gene_prediction/gffParse.faa taxonomy.dat uniprot_sprot.dat > scaffolds.txt
```

```bash
python removeContigs.py ../02_Haemoproteus_clean_genome_sequence/Haemoproteus_tartakovskyi.cleaned.genome scaffolds.txt Ht.final.genome
```

Gene prediction after the avian genes have been removed:
```bash
gmes_petap.pl --ES --cores 100 --min_contig 10000 --sequence Ht.final.onlyid.genome
```

Clean the fasta headers with AWK:
```bash
awk '/^>/ {print $1; next} { print $0}' ../04_Haemoproteus_blast/Ht.final.genome > ../04_Haemoproteus_blast/Ht.final.onlyid.genome
```

Create the fasta files from the final version of the genome:
```bash
../03_Haemoproteus_gene_prediction/gffParse.pl -F -c -p -i ../04_Haemoproteus_blast/Ht.final.genome -g genemark.gtf -b Ht
```

Create the symbolic links in the resource folder:
```bash
ln -s ../05_Haemoproteus_new_gene_prediction/Ht.faa .
ln -s ../05_Haemoproteus_new_gene_prediction/Ht.fna .
```

| Species                   | Genome Size | Genes | Genomic GC |
| ------------------------- | ----------- | -----|--------|
| Plasmodium berghei        | 17954629    |                                     
| Plasmodium cynomolgi      | 26181343    |
| Plasmodium falciparum     | 23270305    |
| Plasmodium knowlesi       | 23462346    |
| Plasmodium vivax          | 27007701    |
| Plasmodium yoelii         | 22222369    |
| Haemoproteus tartakovskyi |             |
| Toxoplasma gondii         | 128105889   |

## Creating the faa and fna files for every genome

```bash
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Plasmodium_berghei.genome -g genemark.Pb.gtf -b Pb
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Plasmodium_cynomolgi.genome -g genemark.Pc.gtf -b Pc
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Plasmodium_falciparum.genome -g genemark.Pf.gtf -b Pf
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Plasmodium_knowlesi.genome -g fixed_Pk.gtf -b Pk
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Plasmodium_vivax.genome -g genemark.Pv.gtf -b Pv
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Plasmodium_yoelii.genome -g genemark.Py.gtf -b Py
../03_Haemoproteus_gene_prediction/gffParse.pl -c -p -i Toxoplasma_gondii.genome -g Tg.gff -b Tg
```

Leave only the ids in the fasta files:

```bash
awk '/^>/ {print $1; next} { print $0}' input.faa  > output.faa
```

## Proteinortho

Execute proteinortho:
```bash
proteinortho6.pl ../00_resources/{Ht,Pb,Pc,Pf,Pk,Pv,Py,Tg}.cleaned.faa
```

## BUSCO

```bash
busco -c 100 -i ../00_resources/Pb.faa -o Pb -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Pc.faa -o Pc -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Pf.faa -o Pf -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Pk.faa -o Pk -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Pv.faa -o Pv -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Py.faa -o Py -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Tg.faa -o Tg -m prot -l apicomplexa
busco -c 100 -i ../00_resources/Ht.faa -o Ht -m prot -l apicomplexa
```

Create the fasta files with the homologous genes from the busco ouput with the `create_gene_fasta_files.py` script.

## Clustalo

```bash
ls ../08_gene_fasta_files/*.faa | while read file; do clustalo -i "$file" -o "$(basename $file .faa)_aligned.faa" -v --threads 100; done
```

## Raxml

```bash
ls ../09_clustalo_busco/*.faa | while read file; do raxmlHPC -s "$file" -n "$(basename $file _aligned.faa).tre" -o Tg -m PROTGAMMABLOSUM62 -p 12345; done
```

## Create a consensus tree

`consense` takes in input a single file therefore the trees must be concatenated in a single file:

```bash
cat RAxML_bestTree.*.tre > all_trees.tre
```

Call consense on the file with the concatenated trees, specify that the outgroup is *Taxoplasma gondii*:

```bash
echo -e "../10_raxmlHPC_busco/all_trees.tre\nO\n8\nY" | consense
```

Create a png file from the Newick file:

```bash
plottree outtree -o outtree
```

![Consensus Tree](./11_consensus_tree/outtree.png "Consensus Tree")
