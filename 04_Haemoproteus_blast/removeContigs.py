#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from Bio import SeqIO

fasta_file = sys.argv[1]  # Input fasta file
remove_file = sys.argv[2]  # Input wanted file, one gene name per line
result_file = sys.argv[3]  # Output fasta file

with open(result_file, "w") as f, open(remove_file, "r") as r:
    remove = r.read().split("\n")

    for seq in SeqIO.parse(fasta_file, 'fasta'):
        if seq.id not in remove:
            SeqIO.write(seq, f, "fasta")