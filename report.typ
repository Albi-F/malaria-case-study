Case Study: The origin of human malaria

1. Yes, I think that parasites that use similar hosts are more likely to be closely related than parasites that use different hosts. This is because parasites that use similar hosts are more likely to have similar characteristics and are more likely to have evolved from a common ancestor. 

